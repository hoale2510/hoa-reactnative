import {ImageSourcePropType} from 'react-native';
const API_URL = 'https://api.themoviedb.org/3';

export type Hotel = {
  id: number;
  name: string;
  price: number;
  location: string;
  image: ImageSourcePropType;
  details: string;
  title: string;
  overview: string;
  original_title: string;
  release_date: string;
  poster_path: string;
};
const newAPI = {
  async fetchAll() {
    const result = await fetch(
      `${API_URL}/movie/popular?api_key=65302d9fd57dda3ea2ba86f370ab6b7f`,
    );
    return result.json();
  },
  async fetchDetail(id: number) {
    const result = await fetch(
      `${API_URL}/movie/${id}?api_key=65302d9fd57dda3ea2ba86f370ab6b7f`,
    );
    return result.json();
  },
};
export default newAPI;
