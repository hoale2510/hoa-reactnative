import React from 'react';
import {
  StyleSheet,
  Text,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';
import COLORS from '../api/color';

export type Props = {
  style?: ViewStyle | ViewStyle[];
  buttonText: string;
  textStyle?: TextStyle;
  onPress?: () => void;
  disable?: boolean;
};

const MyButton: React.FC<Props> = ({
  style = styles.button,
  buttonText = '??',
  textStyle = styles.highlight,
  onPress,
  disable = false,
}) => {
  return (
    <TouchableOpacity disabled={disable} style={style} onPress={onPress}>
      <Text style={textStyle}>{buttonText}</Text>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  button: {
    padding: 10,
    width: '50%',
    marginHorizontal: 10,
    marginTop: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: COLORS.dark,
    alignItems: 'center',
  },
  highlight: {
    fontWeight: '700',
    color: COLORS.orange,
  },
});

export default MyButton;

export {MyButton as MyButton2};
