﻿import * as React from 'react';
import {Text, Image, FlatList, StyleSheet} from 'react-native';

interface ServiceListProps {
  category: {
    id: string;
    title: string;
    movies: {
      id: string;
      poster: string;
    }[];
  };
}

const ServiceList = (props: ServiceListProps) => {
  const {category} = props;
  return (
    <>
      <Text style={styles.title}>{category.title}</Text>
      <FlatList
        data={category.movies}
        renderItem={({item}) => (
          <Image style={styles.image} source={{uri: item.poster}} />
        )}
        horizontal
      />
    </>
  );
};

export default ServiceList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  image: {
    width: 100,
    height: 170,
    resizeMode: 'cover',
    borderRadius: 5,
    margin: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
