import React from 'react';
import {StyleSheet, Text, View, SafeAreaView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../api/color';
import {MainStackScreenProps} from '../../stacks/Navigation';

import TabViewExample from './HomeTabView';

export const HomeScreen: React.FC<MainStackScreenProps<'Home'>> = ({}) => {
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.container}>
        <View style={styles.viewHeader}>
          <Text style={styles.text}> Find your services</Text>
          <View style={styles.viewHeader2}>
            <Text style={styles.text}> in </Text>
            <Text style={[styles.text, styles.color]}>Paris</Text>
          </View>
        </View>
        <Icon name="person-outline" size={38} color={COLORS.grey} />
      </View>
      <TabViewExample />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  container: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  viewHeader: {
    paddingBottom: 15,
  },
  viewHeader2: {
    flexDirection: 'row',
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  color: {
    color: COLORS.primary,
  },
  viewTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
});
