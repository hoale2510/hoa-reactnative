import React, {useCallback, useMemo} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import COLORS from '../../api/color';
import {Hotel} from '../../api/services';
import StarsElements from '../../components/Stars';

export type Props = {
  item: Hotel;
  onPress?: (item: Hotel) => void;
};
export default function HomeItem({item, onPress}: Props) {
  const HomeImage = useMemo(
    () => ({
      uri: `https://image.tmdb.org/t/p/w500${item?.poster_path}`,
    }),
    [item],
  );
  console.log('HomeItem', item);
  console.log('homeImage', HomeImage);
  const localPress = useCallback(() => {
    onPress && onPress(item);
  }, [item, onPress]);

  return (
    <TouchableOpacity onPress={localPress}>
      <View>
        <View style={styles.categoryListContainer}>
          <View style={styles.cardPrice}>
            <Text style={styles.cardTextPrice}>${item.price}</Text>
          </View>
          <Image style={styles.cardImage} source={HomeImage} />
          <View style={styles.text}>
            <Text style={styles.cardDetail} ellipsizeMode={'tail'}>
              {item?.original_title}
            </Text>
            <View>
              <Text
                numberOfLines={6}
                ellipsizeMode={'tail'}
                style={styles.cardName}>
                {item?.overview}
              </Text>
            </View>
            <View style={styles.start}>
              <StarsElements votes={5} size={20} color={COLORS.orange} />
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export const styles = StyleSheet.create({
  categoryListContainer: {
    flexDirection: 'row',
  },
  text: {
    marginTop: 20,
    marginBottom: 15,
  },
  cardImage: {
    height: 150,
    marginTop: 20,
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    width: '50%',
    borderRadius: 15,
  },
  cardName: {
    height: 90,
    marginTop: 10,
    color: COLORS.dark,
    fontSize: 10,
    fontWeight: 'bold',
  },
  cardDetail: {
    width: '37%',
    color: COLORS.primary,
    fontSize: 20,
  },
  cardPrice: {
    height: 50,
    width: 60,
    backgroundColor: COLORS.primary,
    position: 'absolute',
    zIndex: 1,
    marginTop: 20,
    marginLeft: 10,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardTextPrice: {
    color: COLORS.white,
    fontSize: 20,
    fontWeight: 'bold',
  },
  start: {
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
