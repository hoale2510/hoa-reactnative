import React from 'react';
import {StyleSheet} from 'react-native';
import {HomeScreen} from '../screens/Home/HomeScreen';
import {MainStackParamList, TabStackScreenProps} from '../stacks/Navigation';
import {
  BottomTabBarOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RequestScreen from './Requests/RequestScreen';
import NewScreen from './New/NewScreen';

const Tab = createBottomTabNavigator<MainStackParamList>();
const styles = StyleSheet.create({
  tabBarOption: {
    alignItems: 'stretch',
  },
});
const tabBarOptions: BottomTabBarOptions = {
  activeTintColor: 'red',
  showLabel: true,
  style: styles.tabBarOption,
};

export const MainTab: React.FC<TabStackScreenProps<'MainTab'>> = props => {
  console.log(props);
  return (
    <Tab.Navigator tabBarOptions={tabBarOptions}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="New"
        component={NewScreen}
        options={{
          tabBarLabel: 'New',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="newspaper"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Requests"
        component={RequestScreen}
        options={{
          tabBarLabel: 'Requests',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="chat" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTab;
