import React, {useCallback, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Hotel} from '../../api/services';
import {fetchNews} from '../../redux/newSlice';

import {AppDispatch, RootState} from '../../redux/store';
import {MainStackScreenProps} from '../../stacks/Navigation';
import NewScreenList from './NewScreenList';
const NewScreen: React.FC<MainStackScreenProps<'New'>> = ({navigation}) => {
  const dispatch: AppDispatch = useDispatch();
  const {hotels, isFetching} = useSelector((state: RootState) => state?.new);

  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  const onMovieItemPress = useCallback(
    (item: Hotel) => {
      navigation.navigate('NewDetail', {id: item.id});
    },
    [navigation],
  );

  return (
    <NewScreenList
      onPress={onMovieItemPress}
      isFetching={isFetching}
      hotels={hotels}
    />
  );
};
export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default NewScreen;
