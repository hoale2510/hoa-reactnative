import {RouteProp, useRoute} from '@react-navigation/core';
import React, {useEffect} from 'react';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fetchNew} from '../../redux/newSlice';
import {AppDispatch, RootState} from '../../redux/store';
import {
  MainStackParamList,
  MainStackScreenProps,
} from '../../stacks/Navigation';
import NewItem from './NewScreenItem';
const NewScreenDetail: React.FC<MainStackScreenProps<'NewDetail'>> = ({
  route,
}) => {
  const route2 = useRoute<RouteProp<MainStackParamList, 'NewDetail'>>();
  const dispatch: AppDispatch = useDispatch();
  const {hotel, isFetchingDetail, errorDetail} = useSelector(
    (state: RootState) => state?.new,
  );

  console.log(route?.params?.id);

  useEffect(() => {
    if (route2?.params?.id) {
      dispatch(fetchNew(route2?.params?.id));
    }
  }, [dispatch, route2?.params?.id]);

  return (
    <View style={styles.container}>
      {isFetchingDetail ? (
        <ActivityIndicator color="red" />
      ) : hotel ? (
        <NewItem item={hotel} />
      ) : (
        <Text>{errorDetail?.toString()}</Text>
      )}
      <Text> New Screen Detail</Text>
    </View>
  );
};
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
});
export default NewScreenDetail;
