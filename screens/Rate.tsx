import React, {useState} from 'react';
import {StyleSheet, View, Text, Image, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ThemeButton from '../components/ThemeButton';
import {MainStackScreenProps} from '../stacks/Navigation';
import RNTextArea from '@freakycoder/react-native-text-area';
import COLORS from '../api/color';

export const RateScreen: React.FC<MainStackScreenProps<'Rate'>> = ({
  navigation,
}) => {
  const [defaultRating, setdefaultRating] = useState(0);
  const [maxRating] = useState([1, 2, 3, 4, 5]);

  const startImgFilled =
    'https://github.com/tranhonghan/images/blob/main/star_filled.png?raw=true';
  const startImgCorner =
    'https://github.com/tranhonghan/images/blob/main/star_corner.png?raw=true';

  return (
    <View style={styles.container}>
      <Text style={styles.textStar}>Please Rate</Text>
      <View style={styles.customRating}>
        {maxRating.map(item => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              key={item}
              onPress={() => setdefaultRating(item)}>
              <Image
                style={styles.starImg}
                source={
                  item <= defaultRating
                    ? {uri: startImgFilled}
                    : {uri: startImgCorner}
                }
              />
            </TouchableOpacity>
          );
        })}
      </View>

      <RNTextArea
        style={styles.viewFeedBack}
        maxCharLimit={50}
        placeholderTextColor="black"
        exceedCharCountColor="#990606"
        placeholder={'Write your review...'}
        onChangeText={(text: string) => console.log('Text: ', text)}
      />

      <View style={styles.starButton}>
        <ThemeButton
          buttonText="Submit"
          onPress={() => {
            navigation.replace('MainTab');
            Alert.alert('Thank you for using the hotel service');
          }}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
  },
  textStar: {
    textAlign: 'center',
    fontSize: 23,
  },
  customRating: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 30,
  },
  starImg: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
  },
  starButton: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    padding: 15,
  },
  textInput: {
    height: 150,
    borderRadius: 16,
  },
  touch: {
    height: 50,
    width: '95%',
    borderRadius: 16,
    backgroundColor: 'white',
  },
  linear: {
    height: 50,
    width: '100%',
    borderRadius: 16,
    alignContent: 'center',
    justifyContent: 'center',
  },
  viewFeedBack: {
    height: '40%',
    marginTop: 30,
    borderRadius: 30,
    backgroundColor: COLORS.white,
  },
});
