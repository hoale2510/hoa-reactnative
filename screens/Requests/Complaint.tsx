import * as React from 'react';
import {View, StyleSheet} from 'react-native';
// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {MainStackScreenProps} from '../../stacks/Navigation';
import Input from '../../components/Input';
import ThemeButton from '../../components/ThemeButton';
// import {useForm} from 'react-hook-form';

type FormData = {
  name: string;
  email: string;
  password: string;
};

export const ComplaintScreen: React.FC<MainStackScreenProps<'Complaint'>> = ({
  navigation,
}) => {
  // const { handleSubmit, register, setValue, errors } = useForm<FormData>();
  // const onSubmit = useCallback(() => {
  // Alert.alert('Your feedback has been submitted');
  //   navigation.navigate('Status');
  // }, [navigation]);
  return (
    <View style={styles.formContainer}>
      <Input name="room" label="Room" />
      <Input name="title" label="Title" />
      <Input name="context" label="Context" />
      <View style={styles.complaintButton}>
        <ThemeButton
          buttonText="Submit"
          onPress={() => {
            navigation.navigate('Status');
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'grey',
  },
  formContainer: {
    padding: 8,
    flex: 1,
  },
  complaintButton: {
    alignItems: 'center',
    marginTop: 15,
  },
});
