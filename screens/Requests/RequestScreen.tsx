import React from 'react';
import {View} from 'react-native';
import ThemeButton from '../../components/ThemeButton';
import useThemeStyle from '../../hooks/useThemeStyle';
import {MainStackScreenProps} from '../../stacks/Navigation';

const RequestScreen: React.FC<MainStackScreenProps<'Requests'>> = ({
  navigation,
}) => {
  const theme = useThemeStyle();
  return (
    <View style={theme.backgroundStyle}>
      <ThemeButton
        buttonText="Services Extra"
        onPress={() => {
          navigation.navigate('ServicesExtra');
        }}
      />
      <ThemeButton
        buttonText="Services General"
        onPress={() => {
          navigation.navigate('ServicesGeneral');
        }}
      />
      <ThemeButton
        buttonText="Complaint"
        onPress={() => {
          navigation.navigate('Complaint');
        }}
      />
    </View>
  );
};

export default RequestScreen;
