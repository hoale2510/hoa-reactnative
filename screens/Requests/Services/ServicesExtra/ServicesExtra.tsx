import React, {useCallback, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Hotel} from '../../../../api/services';
import {fetchNews} from '../../../../redux/newSlice';
import {AppDispatch, RootState} from '../../../../redux/store';
import {MainStackScreenProps} from '../../../../stacks/Navigation';
import ServicesScreenListExtra from './ServicesExtraList';

const ServicesScreenExtra: React.FC<MainStackScreenProps<'ServicesExtra'>> = ({
  navigation,
}) => {
  const dispatch: AppDispatch = useDispatch();
  const {hotels, isFetching} = useSelector((state: RootState) => state?.new);
  useEffect(() => {
    dispatch(fetchNews());
  }, [dispatch]);

  const onServicesItemPress = useCallback(
    (item: Hotel) => {
      navigation.navigate('Complaint', {id: item.id});
    },
    [navigation],
  );

  return (
    <ServicesScreenListExtra
      onPress={onServicesItemPress}
      isFetching={isFetching}
      hotels={hotels}
    />
  );
};
export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default ServicesScreenExtra;
