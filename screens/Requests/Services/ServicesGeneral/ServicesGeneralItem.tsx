import React, {useCallback, useMemo} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import COLORS from '../../../../api/color';
import {Hotel} from '../../../../api/services';
import StarsElements from '../../../../components/Stars';

export type Props = {
  item: Hotel;
  onPress?: (item: Hotel) => void;
};
export default function ServicesScreenItemGeneral({item, onPress}: Props) {
  const NewImage = useMemo(
    () => ({
      uri: `https://image.tmdb.org/t/p/w500${item?.poster_path}`,
    }),
    [item],
  );
  console.log('ServicesScreenItemGeneral', item);
  console.log('newImage', NewImage);
  const localPress = useCallback(() => {
    onPress && onPress(item);
  }, [item, onPress]);

  return (
    <TouchableOpacity onPress={localPress}>
      <View>
        <View style={styles.categoryListContainer}>
          <View style={styles.cardPrice}>
            <Text style={styles.cardTextPrice}>${item.price}</Text>
          </View>
          <Image style={styles.cardImage} source={NewImage} />
          <View style={styles.text}>
            <Text style={styles.cardDetail} ellipsizeMode={'tail'}>
              {item?.original_title}
            </Text>

            <View>
              <Text
                numberOfLines={6}
                ellipsizeMode={'tail'}
                style={styles.cardName}>
                {item?.overview}
              </Text>
            </View>
            <View style={styles.start}>
              <StarsElements votes={5} size={20} color={COLORS.grey} />
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export const styles = StyleSheet.create({
  categoryListContainer: {
    flexDirection: 'row',
  },
  text: {
    marginTop: 20,
    marginBottom: 15,
  },
  cardImage: {
    height: 130,
    marginTop: 20,
    marginBottom: 15,
    marginLeft: 10,
    marginRight: 10,
    width: '40%',
    borderRadius: 15,
  },
  cardName: {
    height: 90,
    marginTop: 10,
    color: COLORS.dark,
    fontSize: 10,
    fontWeight: 'bold',
  },
  cardDetail: {
    marginTop: 10,
    width: '37%',
    color: COLORS.primary,
    fontSize: 20,
  },
  cardPrice: {
    height: 50,
    width: 60,
    backgroundColor: COLORS.primary,
    position: 'absolute',
    zIndex: 1,
    marginTop: 20,
    marginLeft: 10,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardTextPrice: {
    color: COLORS.white,
    fontSize: 20,
    fontWeight: 'bold',
  },
  start: {
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
