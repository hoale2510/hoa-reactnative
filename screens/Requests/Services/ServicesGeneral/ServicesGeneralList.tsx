import React, {useCallback} from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListRenderItemInfo,
  StyleSheet,
  View,
} from 'react-native';
import {Hotel} from '../../../../api/services';
import ServicesItemGeneral from './ServicesGeneralItem';
const SCROLL_INSET = {right: 1};
export type Props = {
  hotels: Hotel[];
  isFetching: boolean;
  onPress: (item: Hotel) => void;
};
const ServicesScreenListGeneral = (props: Props) => {
  const keyExtractor = useCallback(
    (item: Hotel, index: number): string => (item?.id ?? 0 + index).toString(),
    [],
  );
  const renderItem = useCallback(
    ({item}: ListRenderItemInfo<Hotel>) => (
      <ServicesItemGeneral onPress={props.onPress} item={item} />
    ),
    [props.onPress],
  );

  return (
    <View style={styles.container}>
      {props.isFetching ? (
        <ActivityIndicator color="red" size="large" />
      ) : (
        <FlatList
          scrollIndicatorInsets={SCROLL_INSET}
          data={props.hotels}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};
export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default ServicesScreenListGeneral;
