import {View} from 'native-base';
import {StyleSheet, Text} from 'react-native';
import React, {useState} from 'react';
import {MainStackScreenProps} from '../stacks/Navigation';
import ThemeButton from '../components/ThemeButton';
import {RadioButton} from 'react-native-paper';

export const StatusScreen: React.FC<MainStackScreenProps<'Status'>> = ({
  navigation,
}) => {
  const [checked, setChecked] = useState('first');
  return (
    <View style={styles.container}>
      <View style={styles.statusText}>
        <RadioButton
          value="first"
          status={checked === 'first' ? 'checked' : 'unchecked'}
          onPress={() => setChecked('first')}
        />
        <Text style={styles.textDone}>Done</Text>
      </View>
      <View style={styles.statusText}>
        <RadioButton
          value="second"
          status={checked === 'second' ? 'checked' : 'unchecked'}
          onPress={() => setChecked('second')}
        />
        <Text style={styles.textNoDone}> Non Done</Text>
      </View>
      <View style={styles.statusButton}>
        <ThemeButton
          buttonText="Submit"
          onPress={() => {
            navigation.navigate('Rate');
          }}
        />
      </View>
    </View>
  );
};

export default StatusScreen;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  statusText: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20,
  },
  statusCheckBox: {
    marginTop: 5,
  },
  textDone: {
    marginLeft: 20,
    fontSize: 20,
  },
  textNoDone: {
    marginLeft: 20,
    fontSize: 20,
  },
  statusButton: {
    alignItems: 'center',
    marginTop: 15,
  },
});
