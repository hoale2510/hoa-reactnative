import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {LoginScreen} from '../screens/LoginScreen';

const Stack = createStackNavigator();
const LoginStack = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen name="Login" component={LoginScreen} />
    </Stack.Navigator>
  );
};

export default LoginStack;
