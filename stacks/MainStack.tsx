import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {HomeScreen} from '../screens/Home/HomeScreen';
import HomeScreenDetail from '../screens/Home/HomeScreenDetail';
import MainTab from '../screens/MainTabScreen';
import NewScreenDetail from '../screens/New/NewScreenDetail';
import {RateScreen} from '../screens/Rate';
import {ComplaintScreen} from '../screens/Requests/Complaint';
import RequestScreen from '../screens/Requests/RequestScreen';
import ServicesScreenExtra from '../screens/Requests/Services/ServicesExtra/ServicesExtra';
import ServicesScreenGeneral from '../screens/Requests/Services/ServicesGeneral/ServicesGeneral';
import StatusScreen from '../screens/Status';
import {MainStackParamList, TabStackParamList} from './Navigation';

const Stack = createStackNavigator<TabStackParamList & MainStackParamList>();
const MainStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainTab"
        component={MainTab}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="HomeDetail" component={HomeScreenDetail} />
      <Stack.Screen name="NewDetail" component={NewScreenDetail} />
      <Stack.Screen name="Requests" component={RequestScreen} />
      <Stack.Screen name="ServicesExtra" component={ServicesScreenExtra} />
      <Stack.Screen name="ServicesGeneral" component={ServicesScreenGeneral} />
      <Stack.Screen name="Complaint" component={ComplaintScreen} />
      <Stack.Screen name="Status" component={StatusScreen} />
      <Stack.Screen name="Rate" component={RateScreen} />
    </Stack.Navigator>
  );
};

export default MainStack;
