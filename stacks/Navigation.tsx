import React, {useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {AuthUserContext} from '../contexts/AuthUserProvider';
import LoginStack from './LoginStack';
import {StackNavigationProp, StackScreenProps} from '@react-navigation/stack';

import {
  BottomTabNavigationProp,
  BottomTabScreenProps,
} from '@react-navigation/bottom-tabs';
import MainStack from './MainStack';

type HomeProps = {
  id: string;
};
type HomeDetailProps = {
  id: number;
};
type NewDetailProps = {
  id: number;
};
type ComplaintProps = {
  id: number;
};
export type TabStackParamList = {
  MainTab: undefined;
};
export type T = keyof TabStackParamList;

export type TabStackScreenProps<RouteName extends T> = BottomTabScreenProps<
  TabStackParamList,
  RouteName
>;

export type TabStackNavigation = BottomTabNavigationProp<TabStackParamList>;

export type AuthStackParamList = {
  Login: undefined;
};
export type MainStackParamList = {
  Home: HomeProps | undefined;
  HomeDetail: HomeDetailProps | undefined;
  New: undefined;
  NewDetail: NewDetailProps | undefined;
  Requests: undefined;
  ServicesExtra: undefined;
  ServicesGeneral: undefined;
  Complaint: ComplaintProps | undefined;
  TabView: undefined;
  Status: undefined;
  Rate: undefined;
};

export type MainStackNavigation = StackNavigationProp<MainStackParamList>;
export type S = keyof MainStackParamList;
export type MainStackScreenProps<RouteName extends S> = StackScreenProps<
  MainStackParamList,
  RouteName
>;

export const AppStack = () => {
  const auth = useContext(AuthUserContext);
  return (
    <NavigationContainer>
      {auth.isAuth ? <MainStack /> : <LoginStack />}
    </NavigationContainer>
  );
};
